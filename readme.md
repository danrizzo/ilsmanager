# ILSManager

Questo è il gestionale interno dell'associazione Italian Linux Society.

## Installazione
### Dipendenze

* PHP PDO

### Installazione
```
composer install
cp .env.example .env
vim .env
php artisan migrate
php artisan key:generate
php artisan db:seed
```

### Avvio
```
php artisan serve
```

