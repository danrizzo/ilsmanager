<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\AccountRule;
use App\Bank;

class RuleController extends Controller
{
    public function update(Request $request, $id)
    {
        $this->checkAuth();

        DB::beginTransaction();
        $bank = Bank::find($id);

        $ids = $request->input('rule_id', []);
        $rules = $request->input('rule', []);
        $notes = $request->input('notes', []);
        $accounts = $request->input('account', []);

        foreach($ids as $index => $id) {
            $rule = trim($rules[$index]);
            if (empty($rule)) {
                continue;
            }

            if ($id == 'new') {
                $r = new AccountRule();
            }
            else {
                $r = AccountRule::find($id);
            }

            $r->bank_id = $bank->id;
            $r->rule = $rule;
            $r->account_id = $accounts[$index];
            $r->notes = $notes[$index] ?? '';
            $r->save();
        }

        $remove = $request->input('remove', []);
        AccountRule::whereIn('id', $remove)->delete();

        DB::commit();
        return redirect()->route('bank.edit', $bank->id);
    }
}
