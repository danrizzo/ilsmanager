<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DocumentationController extends Controller
{
    public function index(Request $request)
    {
        $folder = $request->input('folder', '');
        if ($folder == '/')
            $folder = '';

        $path = storage_path('documentation' . $folder);
        $dir = scandir($path);
        $files = [];

        foreach($dir as $f) {
            if ($f[0] == '.')
                continue;

            $files[] = (object) [
                'name' => $f,
                'directory' => is_dir($path . '/' . $f)
            ];
        }

        return view('documentation.index', compact('folder', 'files'));
    }

    public function store(Request $request)
    {
        $this->checkAuth();

        $folder = $request->input('folder', '');
        if ($folder == '/')
            $folder = '';

        $path = storage_path('documentation' . $folder);

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $file->move($path, $file->getClientOriginalName());
        }
        else {
            $name = $request->input('name');
            mkdir($path . '/' . $name, 0770);
        }

        return redirect()->route('doc.index', ['folder' => $folder]);
    }

    public function download(Request $request)
    {
        $file = $request->input('file');
        $file = str_replace('..', '', $file);
        $path = storage_path('documentation' . $file);
        return response()->download($path);
    }
}
