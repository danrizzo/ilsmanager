<?php

namespace App\Observers;

use Mail;
use Log;

use App\Receipt;

class ReceiptObserver
{
    public function creating(Receipt $receipt)
    {
        list($year, $month, $day) = explode('-', $receipt->date);
        $receipt->number = Receipt::whereRaw("YEAR(date) = $year")->max('number') + 1;
    }
}
