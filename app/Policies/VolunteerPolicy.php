<?php

namespace App\Policies;

use App\User;
use App\Volunteer;
use Illuminate\Auth\Access\HandlesAuthorization;

class VolunteerPolicy
{
    use HandlesAuthorization;

    public function view(User $user)
    {
        return $user->hasRole('admin');
    }

    public function create(User $user)
    {
        return $user->hasRole('admin') || $user->hasRole('referent');
    }

    public function update(User $user, Volunteer $volunteer)
    {
        return $user->hasRole('admin') || ($volunteer->section_id == $user->section_id && $user->hasRole('referent'));
    }

    public function delete(User $user, Volunteer $volunteer)
    {
        return $user->hasRole('admin') || ($volunteer->section_id == $user->section_id && $user->hasRole('referent'));
    }
}
