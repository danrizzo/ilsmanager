<?php

namespace App\Policies;

use App\User;
use App\Section;
use Illuminate\Auth\Access\HandlesAuthorization;

class SectionPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Section $section)
    {
        return $user->hasRole('admin') || $user->section_id == $section->id;
    }

    public function create(User $user)
    {
        return $user->hasRole('admin');
    }

    public function update(User $user, Section $section)
    {
        return $user->hasRole('admin') || ($user->section_id == $section->id && $user->hasRole('referent'));
    }

    public function delete(User $user, Section $section)
    {
        return $user->hasRole('admin');
    }
}
