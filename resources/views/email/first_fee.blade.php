<p>
    È stato registrato il versamento della tua quota: benvenuto in {{ App\Config::getConfig('association_name') }}!
</p>
<p>
    Da adesso puoi consultare lo stato della tua registrazione e modificare il tuo profilo accedendo al nostro gestionale soci. Dalla pagina<br>
    <a href="{{ route('password.request') }}">{{ route('password.request') }}</a><br>
    puoi impostare la tua nuova password di accesso, e da<br>
    <a href="{{ route('login') }}">{{ route('login') }}</a><br>
    puoi effettuare il login. Il tuo nickname è "{{ $user->username }}".
</p>

@if(App\Config::getConfig('custom_email_aliases') == '1') {
    <p>
        Nelle prossime ore sarà automaticamente attivato il tuo alias di posta {{ $user->custom_email }}, che puoi configurare sempre dal gestionale soci.
    </p>
@endif

<p>
    Grazie per la tua adesione a {{ App\Config::getConfig('association_name') }}!
</p>
