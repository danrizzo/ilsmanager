@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item {{ empty($folder) ? 'active' : '' }}"><a href="{{ route('doc.index') }}">Documentazione</a></li>
                    <?php $accumulate = [] ?>
                    @foreach(explode('/', $folder) as $bread)
                        <?php $accumulate[] = $bread ?>
                        @if(empty($bread))
                            @continue
                        @endif
                        <li class="breadcrumb-item"><a href="{{ route('doc.index', ['folder' => join('/', $accumulate)]) }}">{{ $bread }}</a></li>
                    @endforeach
                </ol>
            </nav>
        </div>
    </div>

    @if($currentuser->hasRole('admin'))
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createFile">Crea Nuovo File</button>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createFolder">Crea Nuova Cartella</button>

                <div class="modal fade" id="createFile" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Crea Nuovo File in {{ $folder ?: '/' }}</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ route('doc.store') }}" enctype="multipart/form-data">
                                @csrf

                                <div class="modal-body">
                                    <input type="hidden" name="folder" value="{{ $folder }}">

                                    <div class="form-group row">
                                        <label for="file" class="col-sm-4 col-form-label">File</label>
                                        <div class="col-sm-8">
                                            <input type="file" class="form-control" name="file" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary">Salva</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="createFolder" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Crea Nuova Cartella in {{ $folder ?: '/' }}</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ route('doc.store') }}">
                                @csrf

                                <div class="modal-body">
                                    <input type="hidden" name="folder" value="{{ $folder }}">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Nome</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="name" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary">Salva</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row mt-3">
        <div class="col-md-12">
            <table class="table">
                <tbody>
                    @if(!empty($folder))
                        <tr>
                            <td><a href="{{ route('doc.index', ['folder' => dirname($folder)]) }}">..</a></td>
                            <td></td>
                        </tr>
                    @endif

                    @foreach($files as $file)
                        <tr>
                            <td>
                                @if($file->directory == false)
                                    {{ $file->name }}
                                @else
                                    <a href="{{ route('doc.index', ['folder' => $folder . '/' . $file->name]) }}">{{ $file->name }}</a>
                                @endif
                            </td>
                            <td>
                                @if($file->directory == false)
                                    <a href="{{ route('doc.download', ['file' => $folder . '/' . $file->name]) }}"><span class="oi oi-data-transfer-download"></span></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
