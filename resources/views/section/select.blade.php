<?php

if (!isset($name))
    $name = 'section[]';

if (!isset($select))
    $select = 0;

?>

<select class="form-control" name="{{ $name }}" autocomplete="off">
    <option value="0" {{ $select == 0 ? 'selected' : '0' }}>Nessuna</option>
    @foreach(App\Section::orderBy('city', 'asc')->get() as $section)
        <option value="{{ $section->id }}" {{ $select == $section->id ? 'selected' : '' }}>{{ $section->city }} ({{ $section->prov }})</option>
    @endforeach
</select>
