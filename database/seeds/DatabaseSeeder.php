<?php

use App\Assembly;
use App\Votation;
use App\Vote;
use Illuminate\Database\Seeder;

use App\Role;
use App\User;
use App\Account;
use App\Bank;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Role::all()->isEmpty()) {
            $admin_role = new Role();
            $admin_role->name = 'admin';
            $admin_role->save();

            $member_role = new Role();
            $member_role->name = 'member';
            $member_role->save();

            $referent_role = new Role();
            $referent_role->name = 'referent';
            $referent_role->save();
        }

        if (Account::all()->isEmpty()) {
            $accounts = [
                'Soci' => [
                    'Quote Associative'
                ],
                'Attività Istituzionale' => [
                    'Sponsorizzazioni',
                    'Donazioni'
                ],
                'Beni e Servizi' => [
                    'Stampa',
                    'Materiale Promozionale',
                    'Posta e Spedizioni',
                    'Banche',
                    'Commercialista',
                    'Consulenze',
                    'Servizi Web',
                    'Hardware',
                ],
                'Imposte e Tasse' => []
            ];

            foreach ($accounts as $parent => $sub_accounts) {
                $p = new Account();
                $p->name = $parent;
                $p->parent_id = null;
                $p->save();

                foreach ($sub_accounts as $sa) {
                    $s = new Account();
                    $s->name = $sa;
                    $s->parent_id = $p->id;
                    $s->save();
                }
            }

            Account::where('name', 'Quote Associative')->update(['fees' => true]);
            Account::where('name', 'Donazioni')->update(['donations' => true]);
            Account::where('name', 'Banche')->update(['bank_costs' => true]);
        }

        if (Bank::all()->isEmpty()) {
            $bank = new Bank();
            $bank->name = 'Unicredit';
            $bank->type = 'unicredit';
            $bank->identifier = 'IT74G0200812609000100129899';
            $bank->save();

            $bank = new Bank();
            $bank->name = 'PayPal';
            $bank->type = 'paypal';
            $bank->identifier = 'direttore@linux.it';
            $bank->save();
        }
        if (User::all()->isEmpty()) {
            $user = new User();
            $user->username = 'admin';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'regular';
            $user->name = 'Amministratore';
            $user->surname = 'ILS';
            $user->email = 'admin@example.com';
            $user->notes = '';
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (1,1)");

            $user = new User();
            $user->username = 'member';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'regular';
            $user->name = 'Membro';
            $user->surname = 'ILS';
            $user->email = 'member@example.com';
            $user->notes = '';
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (2,2)");

            $user = new User();
            $user->username = 'referent';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'regular';
            $user->name = 'Referente';
            $user->surname = 'ILS';
            $user->email = 'referent@example.com';
            $user->notes = '';
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (3,3)");

            $user = new User();
            $user->username = 'pending';
            $user->password = Hash::make($user->username);
            $user->status = 'pending';
            $user->type = 'regular';
            $user->name = 'In attesa';
            $user->surname = 'ILS';
            $user->email = 'pending@example.com';
            $user->notes = '';
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (2,4)");

            $user = new User();
            $user->username = 'suspended';
            $user->password = Hash::make($user->username);
            $user->status = 'suspended';
            $user->type = 'regular';
            $user->name = 'Sospeso';
            $user->surname = 'ILS';
            $user->email = 'suspended@example.com';
            $user->notes = '';
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (2,5)");

            $user = new User();
            $user->username = 'expelled';
            $user->password = Hash::make($user->username);
            $user->status = 'expelled';
            $user->type = 'regular';
            $user->name = 'Espulso';
            $user->surname = 'ILS';
            $user->email = 'expelled@example.com';
            $user->notes = '';
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (2,6)");

            $user = new User();
            $user->username = 'dropped';
            $user->password = Hash::make($user->username);
            $user->status = 'dropped';
            $user->type = 'regular';
            $user->name = 'Eliminato';
            $user->surname = 'ILS';
            $user->email = 'dropped@example.com';
            $user->notes = '';
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (2,7)");

            $user = new User();
            $user->username = 'association';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'regular';
            $user->name = 'Associazione';
            $user->surname = 'ILS';
            $user->email = 'association@example.com';
            $user->notes = '';
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (2,8)");
        }
        if (Assembly::all()->isEmpty()) {
            $assembly = new Assembly();
            $assembly->date = '2099-12-31';
            $assembly->status = 'pending';
            $assembly->announce = 'Assemblea futura';
            $assembly->save();

            $assembly = new Assembly();
            $date = new DateTime(now());
            $interval = new DateInterval('P1M');
            $date->add($interval);
            $assembly->date = $date->format('Y-m-d');
            $assembly->status = 'open';
            $assembly->announce = 'Assemblea attiva';
            $assembly->save();

            $assembly = new Assembly();
            $assembly->date = "0001-01-01";
            $assembly->status = 'closed';
            $assembly->announce = 'Assemblea chiusa';
            $assembly->save();
        }
        if (Votation::all()->isEmpty()) {
            $votation = new Votation();
            $votation->assembly_id = 2;
            $votation->question = 'Votazione in attesa';
            $votation->status = 'pending';
            $votation->options = array_filter(array("a","b","c"));
            $votation->save();

            $votation = new Votation();
            $votation->assembly_id = 2;
            $votation->question = 'Votazione in corso';
            $votation->status = 'running';
            $votation->options = array_filter(array("d","e","f"));
            $votation->save();

            $votation = new Votation();
            $votation->assembly_id = 2;
            $votation->question = 'Votazione chiusa';
            $votation->status = 'closed';
            $votation->options = array_filter(array("g","h","i"));
            $votation->save();
        }
        if (Vote::all()->isEmpty()) {
            $vote = new Vote();
            $vote->votation_id = 3;
            $vote->user_id = 1;
            $vote->answer = array_filter(array("g"));
            $vote->save();

            $vote = new Vote();
            $vote->votation_id = 3;
            $vote->user_id = 2;
            $vote->answer = array_filter(array("h"));
            $vote->save();

            $vote = new Vote();
            $vote->votation_id = 3;
            $vote->user_id = 3;
            $vote->answer = array_filter(array("i"));
            $vote->save();

            $vote = new Vote();
            $vote->votation_id = 3;
            $vote->user_id = 4;
            $vote->answer = array_filter(array("g"));
            $vote->save();
        }
    }
}
